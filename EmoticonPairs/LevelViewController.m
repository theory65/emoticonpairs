//
//  LevelViewController.m
//  EmoticonPairs
//
//  Created by A M on 3/31/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import "LevelViewController.h"

@interface LevelViewController ()

@end

@implementation LevelViewController

@synthesize delegate = _delegate;

- (IBAction)easyPressed:(id)sender
{
    [self.delegate selectedLevel:Easy];
}

- (IBAction)mediumPressed:(id)sender
{
    [self.delegate selectedLevel:Medium];
}

- (IBAction)hardPressed:(id)sender
{
    [self.delegate selectedLevel:Hard];
}

@end
