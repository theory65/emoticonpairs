//
//  ScoreViewController.h
//  EmoticonPairs
//
//  Created by A M on 3/30/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScoreViewControllerDelegate

- (void)goToHome:(id)sender;

@end

@interface ScoreViewController : UIViewController

@property (strong, nonatomic) NSString *playerName;
@property (nonatomic) int time;
@property (nonatomic) int pairCount;
@property (nonatomic) int level;

@property (weak, nonatomic) id<ScoreViewControllerDelegate> delegate;

@end
