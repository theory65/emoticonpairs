//
//  HelpViewController.m
//  EmoticonPairs
//
//  Created by A M on 3/31/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (IBAction)backPressed:(id)sender
{
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:^{}];
}

@end
