//
//  LevelViewController.h
//  EmoticonPairs
//
//  Created by A M on 3/31/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameViewController.h"

@protocol LevelViewControllerDelegate

- (void)selectedLevel:(int)level;

@end

@interface LevelViewController : UIViewController

@property (weak, nonatomic) id<LevelViewControllerDelegate> delegate;

@end
