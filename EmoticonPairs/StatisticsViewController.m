//
//  StatisticsViewController.m
//  EmoticonPairs
//
//  Created by A M on 3/31/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import "StatisticsViewController.h"
#import "GameViewController.h"

@interface StatisticsViewController ()

@end

@implementation StatisticsViewController

- (IBAction)backPressed:(id)sender
{
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:^{}];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGPoint easyPoint = CGPointMake(114, 209);
    CGPoint mediumPoint = CGPointMake(402, 209);
    CGPoint hardPoint = CGPointMake(681, 209);
    
    [self statisticsForLevel:Easy withStartPoint:easyPoint];
    [self statisticsForLevel:Medium withStartPoint:mediumPoint];
    [self statisticsForLevel:Hard withStartPoint:hardPoint];
}

- (void)statisticsForLevel:(int)level
            withStartPoint:(CGPoint)point
{
    int i = 0;
    NSArray *players = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d", level]];
    for (NSDictionary *player in players) {
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(point.x, point.y + (i * 40), 91, 21)];
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(point.x + 99, point.y+ (i * 40), 71, 21)];
        UILabel *pairCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(point.x + 178, point.y + (i * 40), 42, 21)];
        
        int time = [[player objectForKey:@"time"] intValue];
        timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", time / 3600, time % 3600 / 60, time % 60];
        pairCountLabel.text = [NSString stringWithFormat:@"%d", [[player objectForKey:@"pair count"] intValue]];
        nameLabel.text = [player objectForKey:@"name"];
        
        [self.view addSubview:timeLabel];
        [self.view addSubview:pairCountLabel];
        [self.view addSubview:nameLabel];
        
        i++;
    }
}

@end
