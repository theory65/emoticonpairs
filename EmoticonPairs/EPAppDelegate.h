//
//  EPAppDelegate.h
//  EmoticonPairs
//
//  Created by A M on 3/24/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
