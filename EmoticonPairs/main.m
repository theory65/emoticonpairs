//
//  main.m
//  EmoticonPairs
//
//  Created by A M on 3/24/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EPAppDelegate class]));
    }
}
