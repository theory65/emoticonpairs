//
//  ScoreViewController.m
//  EmoticonPairs
//
//  Created by A M on 3/30/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import "ScoreViewController.h"

@interface ScoreViewController ()

@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *pairCountLabel;

@end

@implementation ScoreViewController

@synthesize timeLabel = _timeLabel;
@synthesize pairCountLabel = _pairCountLabel;
@synthesize playerName = _playerName;
@synthesize time = _time;
@synthesize pairCount = _pairCount;
@synthesize level = _level;
@synthesize delegate = _delegate;

- (void)setPairCount:(int)pairCount
{
    _pairCount = pairCount;
}

- (void)setTime:(int)time
{
    _time = time;
}

- (IBAction)home:(id)sender {
    [self.delegate goToHome:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self performPlayer];
    [self updateLabel];
}

- (void)updateLabel
{
    self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", self.time / 3600, self.time % 3600 / 60, self.time % 60];
    self.pairCountLabel.text = [NSString stringWithFormat:@"%d", self.pairCount];
    
    int i = 0;
    NSArray *players = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d", self.level]];
    for (NSDictionary *player in players) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(407, 350 + (i++ * 29), 210, 21)];
        label.text = [player objectForKey:@"name"];
        [self.view addSubview:label];
    }
}

- (void)performPlayer
{
    NSArray *players = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d", self.level]] mutableCopy];
    NSDictionary *lastPlayer = [players lastObject];
    if ([players count] < 10) {
        [self addPlayer];
    } else if (self.time > [[lastPlayer objectForKey:@"time"] intValue]) {
        [self addPlayer];
    } else if (self.time == [[lastPlayer objectForKey:@"time"] intValue]) {
        if (self.pairCount > [[lastPlayer objectForKey:@"pair count"] intValue]) {
            [self addPlayer];
        }
    }
}

- (void)addPlayer
{
    NSMutableArray *players = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%d", self.level]] mutableCopy];
    if (!players) {
        players = [[NSMutableArray alloc] init];
    } else if ([players count] >= 10) {
        [players removeLastObject];
    }
    
    NSDictionary *newPlayer = [NSDictionary dictionaryWithObjectsAndKeys:
     self.playerName, @"name",
     [NSNumber numberWithInt:self.time], @"time",
     [NSNumber numberWithInt:self.pairCount], @"pair count", nil];
    [players addObject:newPlayer];
    
    [players sortedArrayUsingComparator:^(id obj1, id obj2) {
        int time1 = [[obj1 objectForKey:@"time"] intValue];
        int time2 = [[obj2 objectForKey:@"time"] intValue];
        if (time1 > time2) {
            return (NSComparisonResult)NSOrderedDescending;
        } else if (time2 > time1){
            return (NSComparisonResult)NSOrderedAscending;
        } else {
            int pairCount1 = [[obj1 objectForKey:@"pair count"] intValue];
            int pairCount2 = [[obj2 objectForKey:@"pair count"] intValue];
            if (pairCount1 > pairCount2) {
                return (NSComparisonResult)NSOrderedDescending;
            } else if (pairCount2 > pairCount1) {
                return (NSComparisonResult)NSOrderedAscending;
            } else {
                return (NSComparisonResult)NSOrderedSame;
            }
        }
    }];
    
    [[NSUserDefaults standardUserDefaults] setObject:players forKey:[NSString stringWithFormat:@"%d", self.level]];
}

@end
