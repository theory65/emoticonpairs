//
//  EPViewController.m
//  EmoticonPairs
//
//  Created by A M on 3/24/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import "EPViewController.h"
#import "GameViewController.h"
#import "LevelViewController.h"

@interface EPViewController () <gameViewControllerDelegate, LevelViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *playerName;
@property (nonatomic) int level;

@end

@implementation EPViewController

@synthesize level = _level;

#define DEFAULT_LEVEL 0

- (int)level
{
    if (!_level) {
        return DEFAULT_LEVEL;
    } else {
        return _level;
    }
}

- (IBAction)startGame:(id)sender
{
    [self performSegueWithIdentifier:@"Start Game" sender:self];
}

- (IBAction)level:(id)sender
{
    [self performSegueWithIdentifier:@"Select Level" sender:self];
}

- (IBAction)statistics:(id)sender
{
    [self performSegueWithIdentifier:@"Show Statistics" sender:self];
}

- (IBAction)help:(id)sender
{
    [self performSegueWithIdentifier:@"Help" sender:self];
}

- (IBAction)about:(id)sender
{
    [self performSegueWithIdentifier:@"About" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Start Game"]) {
        [segue.destinationViewController setPlayerName:self.playerName.text];
        [segue.destinationViewController setLevel:self.level];
        [segue.destinationViewController setDelegate:self];
    } else if ([segue.identifier isEqualToString:@"Select Level"]) {
        [segue.destinationViewController setDelegate:self];
    }
}

- (void)replay:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:^{
        [self performSegueWithIdentifier:@"Start Game" sender:self];
    }];
}

- (void)selectedLevel:(int)level
{
    self.level = level;
    [self dismissViewControllerAnimated:NO completion:^{}];
}

@end
