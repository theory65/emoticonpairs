//
//  GameViewController.m
//  EmoticonPairs
//
//  Created by A M on 3/26/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import "GameViewController.h"
#import "ScoreViewController.h"
#import <CommonCrypto/CommonDigest.h>

@interface GameViewController () <ScoreViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIView *gameView;

@property (strong, nonatomic) UIImage *closeImage;
@property (strong, nonatomic) NSMutableArray *emoticons;
@property (strong, nonatomic) NSMutableArray *emoticonsIsShow;

@property (weak, nonatomic) UIButton *firstButton;
@property BOOL isWaitSecondImage;

@property (strong, nonatomic) IBOutlet UILabel *pairCountLabel;
@property (nonatomic) int pairCount;

@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic) int time;
@property (weak, nonatomic) NSTimer *timer;
@property BOOL isGameStart;

@property int hidEmotionCount;

@end

@implementation GameViewController

@synthesize playerName = _playerName;
@synthesize level = _level;
@synthesize gameView = _gameView;
@synthesize closeImage = _closeImage;
@synthesize emoticons = _emoticons;
@synthesize emoticonsIsShow = _emoticonsIsShow;
@synthesize firstButton = _firstButton;
@synthesize isWaitSecondImage = _isWaitSecondImage;
@synthesize pairCountLabel = _pairCountLabel;
@synthesize pairCount = _pairCount;
@synthesize timeLabel = _timeLabel;
@synthesize time = _time;
@synthesize timer = _timer;
@synthesize hidEmotionCount = _hidEmotionCount;
@synthesize delegate = _delegate;

#define IMAGE_COUNT 20

- (void)setPairCount:(int)pairCount
{
    _pairCount = pairCount;
    self.pairCountLabel.text = [NSString stringWithFormat:@"%d", pairCount];
}

- (void)setTime:(int)time
{
    _time = time;
    [self updateTime];
}

- (void)updateTime
{
    self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", self.time / 3600, self.time % 3600 / 60, self.time % 60];
}

#define EASY_SIZE 4
#define MEDIUM_SIZE 6
#define HARD_SIZE 8
#define EASY_RANGE 204
#define MEDIUM_RANGE 114
#define HARD_RANGE 24

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    self.closeImage = [UIImage imageNamed:@"0.png"];
    
    int size, range;
    if (self.level == Easy) {
        size = EASY_SIZE;
        range = EASY_RANGE;
    } else if (self.level == Medium) {
        size = MEDIUM_SIZE;
        range = MEDIUM_RANGE;
    } else {
        size = HARD_SIZE;
        range = HARD_RANGE;
    }
    
    int emoticonCount = size * size;
    [self prepareEmoticons:emoticonCount];
    
    int k = 0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(276 + range + 90 * j, range + 90 * i, 70, 70);
            [button setTitle:[NSString stringWithFormat:@"%d", k++] forState:UIControlStateNormal];
            [button setImage:self.closeImage forState:UIControlStateNormal];
            [button addTarget:self action:@selector(emoticonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.gameView addSubview:button];
        }
    }
}

#define CLOSE_HIDE_DELAY 1.0

- (IBAction)emoticonPressed:(id)sender
{
    if (!self.isGameStart) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(plusTime) userInfo:nil repeats:YES];
        self.isGameStart = YES;
    }
    
    UIButton *button = (UIButton *)sender;
    int index = [[button currentTitle] intValue];
    
    if (![[self.emoticonsIsShow objectAtIndex:index] boolValue]) {
        [button setImage:[self.emoticons objectAtIndex:index] forState:UIControlStateNormal];
        [self.emoticonsIsShow replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
        
        if (self.isWaitSecondImage) {
            UIButton *firstButton = self.firstButton;
            UIButton *secondButton = button;
            NSString *firstImageHash = [self imageHash:[firstButton imageForState:UIControlStateNormal]];
            NSString *secondImageHash = [self imageHash:[secondButton imageForState:UIControlStateNormal]];
            if ([firstImageHash isEqualToString:secondImageHash]) {
                [self performSelector:@selector(hideImage:) withObject:[NSArray arrayWithObjects:firstButton, secondButton, nil] afterDelay:CLOSE_HIDE_DELAY];
                self.hidEmotionCount += 2;
                if (self.hidEmotionCount == [self.emoticons count]) {
                    [self performSegueWithIdentifier:@"Show Score" sender:self];
                }
            } else {
                [self performSelector:@selector(closeImage:) withObject:[NSArray arrayWithObjects:firstButton, secondButton, nil] afterDelay:CLOSE_HIDE_DELAY];
            }
            self.isWaitSecondImage = NO;
            self.pairCount++;
        } else {
            self.isWaitSecondImage = YES;
            self.firstButton = button;

        }
    }
}

- (void)plusTime
{
    self.time++;
}

- (void)hideImage:(NSArray *)buttons
{
    UIButton *firstButton = [buttons objectAtIndex:0];
    UIButton *secondButton = [buttons objectAtIndex:1];
    firstButton.hidden = YES;
    secondButton.hidden = YES;
}

- (void)closeImage:(NSArray *)buttons
{
    UIButton *firstButton = [buttons objectAtIndex:0];
    UIButton *secondButton = [buttons objectAtIndex:1];
    [firstButton setImage:self.closeImage forState:UIControlStateNormal];
    [secondButton setImage:self.closeImage forState:UIControlStateNormal];
    
    int firstIndex = [[firstButton currentTitle] intValue];
    int secondIndex = [[secondButton currentTitle] intValue];
    [self.emoticonsIsShow replaceObjectAtIndex:firstIndex withObject:[NSNumber numberWithBool:NO]];
    [self.emoticonsIsShow replaceObjectAtIndex:secondIndex withObject:[NSNumber numberWithBool:NO]];
}

- (NSString *)imageHash:(UIImage *)image
{
    unsigned char result[16];
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
    CC_MD5([imageData bytes], [imageData length], result);
    NSString *hash = [NSString stringWithFormat:
                           @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                           result[0], result[1], result[2], result[3],
                           result[4], result[5], result[6], result[7],
                           result[8], result[9], result[10], result[11],
                           result[12], result[13], result[14], result[15]
                           ];
    return hash;
}

- (void)prepareEmoticons:(int)emoticonCount
{
    self.emoticons = [[NSMutableArray alloc] init];
    self.emoticonsIsShow = [[NSMutableArray alloc] init];
    
    int i = 0;
    NSMutableArray *emoticonImages = [self emoticonImages];
    while (self.emoticons.count < emoticonCount) {
        if (i % IMAGE_COUNT == 1) {
            emoticonImages = [self shuffleArray:emoticonImages];
        }
        UIImage *emoticon = [emoticonImages objectAtIndex:i++ % IMAGE_COUNT];
        [self.emoticons addObject:emoticon];
        [self.emoticons addObject:emoticon];
        [self.emoticonsIsShow addObject:[NSNumber numberWithBool:NO]];
        [self.emoticonsIsShow addObject:[NSNumber numberWithBool:NO]];
    }
    
    self.emoticons = [self shuffleArray:self.emoticons];
}

- (NSMutableArray *)emoticonImages
{
    return [NSMutableArray arrayWithObjects:
            [UIImage imageNamed:@"1.png"],
            [UIImage imageNamed:@"2.png"],
            [UIImage imageNamed:@"3.png"],
            [UIImage imageNamed:@"4.png"],
            [UIImage imageNamed:@"5.png"],
            [UIImage imageNamed:@"6.png"],
            [UIImage imageNamed:@"7.png"],
            [UIImage imageNamed:@"8.png"],
            [UIImage imageNamed:@"9.png"],
            [UIImage imageNamed:@"10.png"],
            [UIImage imageNamed:@"11.png"],
            [UIImage imageNamed:@"12.png"],
            [UIImage imageNamed:@"13.png"],
            [UIImage imageNamed:@"14.png"],
            [UIImage imageNamed:@"15.png"],
            [UIImage imageNamed:@"16.png"],
            [UIImage imageNamed:@"17.png"],
            [UIImage imageNamed:@"18.png"],
            [UIImage imageNamed:@"19.png"],
            [UIImage imageNamed:@"20.png"],
            [UIImage imageNamed:@"21.png"],
            [UIImage imageNamed:@"22.png"],
            [UIImage imageNamed:@"23.png"],
            nil];
}

- (NSMutableArray *)shuffleArray:(NSMutableArray *)array
{
    int count = [array count];
    for (int i = 0; i < count; i++) {
        int nElements = count - i;
        int n = (arc4random() % nElements) + i;
        [array exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    return array;
}

- (IBAction)home:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:^{}];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Score"]) {
        ScoreViewController *svc = (ScoreViewController *)[segue destinationViewController];
        [svc setPlayerName:self.playerName];
        [svc setLevel:self.level];
        [svc setTime:self.time];
        [svc setPairCount:self.pairCount];
        [svc setDelegate:self];
    }
}

- (IBAction)replay:(id)sender {
    [self.delegate replay:self];
}

- (void)goToHome:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:^{
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:^{}];
    }];
}

@end
