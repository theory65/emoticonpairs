//
//  GameViewController.h
//  EmoticonPairs
//
//  Created by A M on 3/26/56 BE.
//  Copyright (c) 2556 A M. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol gameViewControllerDelegate
- (void)replay:(id)sender;
@end

enum Level
{
    Easy = 0,
    Medium = 1,
    Hard = 2
};

@interface GameViewController : UIViewController

@property (strong, nonatomic) NSString *playerName;
@property int level;

@property (weak, nonatomic) id<gameViewControllerDelegate> delegate;

@end
